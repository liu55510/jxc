package com.atguigu.jxc;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
@MapperScan("com.atguigu.jxc.dao")
public class JxcApplication {

	public static void main(String[] args) {
		log.info("项目启动");
		SpringApplication.run(JxcApplication.class, args);
	}

}
